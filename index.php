<?php
require('conf.php');
?>
<!DOCTYPE HTML>
<html lang="fr">
<head>
    <title>STUDIT</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="description"
          content="Expert en conception de ressources pédagogiques numériques : exercices personnalisés ou animations scénarisées, en conception de sites web et de plateformes e-learning. Nous nous occupons de tout le cycle de vie de vos projet WEB.">
    <meta name="author" content="STUDIT"/>
    <link rel="stylesheet" href="assets/css/main.min.css"/>
    <link rel="stylesheet" href="assets/css/studit.min.css"/>
    <link rel="icon" type="image/png" href="assets/medias/images/favicon.png"/>
    <link rel="preload" href="assets/css/font-awesome.min.css" as="style">
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="assets/css/ie8.css"/><![endif]-->
</head>
<body>

<!-- Header -->
<header>
    <nav data-menuActif="false">
        <div class="logo-nav">
            <img src="assets/medias/images/logo-studit.svg" alt="Logo de STUDIT"/>
        </div>
        <div class="btnMenu">
            <div><i id="btnIcon" class="fa fa-bars"></i></div>
        </div>
        <ul>
            <li>
                <a href="#ressourcesPedas" title="Ressources pédagogiques" class="aNav">Ressources pédagogiques</a>
            </li>
            <li>
                <a href="#sites" title="Sites web et e-learning" class="aNav">Sites web et e-learning</a>
            </li>
            <li>
                <a href="#esante" title="Projet ETP" class="aNav">Projet ETP</a>
            </li>
            <li>
                <a href="#catalogues" title="Catalogues en ligne" class="aNav">Catalogues en ligne</a>
            </li>
        </ul>
    </nav>
    <img class="logoPrinc noDecoLink" src="assets/medias/images/logo_border.png" alt="logo de STUDIT"/>
    <p>Expert en conception de ressources pédagogiques numériques<br/>et en conception de plateformes e-learning.</p>
</header>

<!-- Main -->
<main>
    <div class="presentation">
        <div class="content" dataLien="#ressourcesPedas">
            <h3>Ressources pédagogiques</h3>
            <p>Aussi appelées widgets, ou animations, ces briques pédagogiques sont destinées à être intégrées dans des
                manuels numériques interactifs et dans des CDROM, dans des sites internet. </p>
        </div>
        <div class="content" dataLien="#sites">
            <h3>Sites web et e-learning</h3>
            <p>Nous créons de nombreux sites, entièrement responsive, basés ou non sur des CMS. En particulier nous
                créons et personnalisons notre propre plateforme e-learning</p>
        </div>
        <div class="content" dataLien="#esante">
            <h3>Éducation thérapeutique</h3>
            <p>Nous menons un projet de numérisation d'atelier d'éducation thérapeutique du patient à destination des
                patients. Le premier parcours de soins en ligne a été lancé en Octobre 2019.</p>
        </div>
        <div class="content" dataLien="#catalogues">
            <h3>Catalogue en ligne</h3>
            <p>Nous vous accompagnons dans la réalisation et la mise en ligne de vos catalogues Web, à partir de PDF ou
                de fichiers de mise en page.</p>
        </div>
    </div>
    <div class="box container">
        <h2>Nos spécialités</h2>
        <section id="ressourcesPedas">
            <h3>Ressources pédagogiques</h3>
            <p>Aussi appelées <strong>widgets</strong>, ou <strong>animations</strong></p>
            <p>Ces briques pédagogiques sont destinées à être intégrées dans des manuels numériques interactifs, dans
                des CDROM et dans des sites internet. Nous les développons en HTML et les rendons compatibles avec les
                supports qui les accueillent. Elles sont de types variés :</p>
            <ul class="default studit">
                <li>Exercices interactifs, suivant des typologies (glisser déposer, relier les items, QCM, mots-croisés,
                    intrus, memory...)
                </li>
                <li>Exercices personnalisés unitaires à la demande</li>
                <li>Animations linéaires scénarisées</li>
            </ul>
            <video width="100%" controls poster="assets/medias/images/poster_montage.png">
                <source src="assets/medias/videos/montage.mp4" type="video/mp4">
                Your browser does not support the video tag.
            </video>
        </section>
        <section id="sites">
            <h3>Sites web et e-learning</h3>
            <p>Nous créons de nombreux sites entièrement responsive, basés des CMS ou from scratch. Nous avons
                aujourd'hui une très bonne maîtrise du framework
                <a target="_blank" href="https://symfony.com/" rel="noopener" title="Site de Symfony">Symfony</a>
                qui nous sert pour une grande partie de nos projets. Nous nous occupons de tout le cycle de vie de vos
                projet WEB
            </p>
            <ul class="studit liste">
                <li>Définition du besoin, analyse du projet,</li>
                <li>Création de maquettes,</li>
                <li>Développement de votre site (frontEnd et backEnd),</li>
                <li>Intégration des ressources,</li>
                <li>Proposition d'hébergement.</li>
            </ul>
            <div class="logoSym">
                <a target="_blank" rel="noopener" title="Site de Symfony" class="noDecoLink"
                   href="https://symfony.com/">
                    <img src="assets/medias/images/symfony.png" alt="Logo de Symfony"/>
                </a>
                <a target="_blank" rel="noopener" title="Site de JQuery" class="noDecoLink" href="https://jquery.com/">
                    <img src="assets/medias/images/logo-JQuery.svg" alt="Logo de JQuery"/>
                </a>
                <a target="_blank" rel="noopener" title="Site de Node JS" class="noDecoLink"
                   href="https://nodejs.org/fr/">
                    <img src="assets/medias/images/logo-node-js.svg" alt="Logo de Node JS"/>
                </a>
            </div>
            <p>Pêle mêle, voici quelques exemple de sites que nous avons développés :</p>
            <ul class="default studit">
                <li>
                    <a target="_blank" href="https://www.afadec.fr/" rel="noopener"
                       title="Plateforme e-learning de l'AFADEC">Plateforme e-learning de l'AFADEC</a>
                </li>
                <li>
                    <a target="_blank" href="https://e-didier.com/" rel="noopener"
                       title="Site E-didier, éditions Didier">E-didier</a>, un site de référencement des ressource
                    numérique des
                    <a target="_blank" href="https://www.editionsdidier.com/fr" rel="noopener"
                       title="Site des éditions Didier">Éditions Didier</a>
                </li>
                <li>
                    <a target="_blank" href="http://didier-richtigclever.com/" rel="noopener"
                       title="Site Richtig Clever, éditions Didier">Richtig Clever</a>, un site compagnon des
                    <a target="_blank" href="https://www.editionsdidier.com/fr" rel="noopener"
                       title="Site des éditions Didier">Éditions Didier</a>
                </li>
                <li>
                    <a target="_blank" href="http://www.didier-nishuoya.com/" rel="noopener"
                       title="Site Ni shuo ya !, éditions Didier">Ni shuo ya !</a>, un site compagnon des
                    <a target="_blank" href="https://www.editionsdidier.com/fr" rel="noopener"
                       title="Site des éditions Didier">Éditions Didier</a>
                </li>
                <li>
                    <a target="_blank" href="http://www.didier-vamosalla.com/" rel="noopener"
                       title="Site Vamos Allà, éditions Didier">Vamos Allà</a>, un site compagnon des
                    <a target="_blank" href="https://www.editionsdidier.com/fr" rel="noopener"
                       title="Site des éditions Didier">Éditions Didier</a>
                </li>
            </ul>
        </section>
        <section id="esante">
            <h3>PROJET : Éducation Thérapeutique du Patient (ETP)</h3>
            <p>Nous menons un projet de numérisation d'atelier d'ETP en partenariat avec le <a target="_blank"
                                                                                               href="https://www.cmpr-bagnoles.com/"
                                                                                               rel="noopener"
                                                                                               title="Site du CMPR de Bagnole de l'Orne Normandie">CMPR
                    de Bagnoles de L'Orne</a>. En Octobre 2019, une première version de parcours de soins a été mise en
                ligne, à destination des patients atteints de TMS-MS (Troubles Musculosquelettiques du Membre
                Supérieur). Cette plateforme permet à des patients qui suivent déjà un atelier d'ETP en présentiel
                d'être accompagnés également par des ressources pédagogiques en ligne, conçues par les thérapeutes et
                mises en forme par STUDIT.</p>
            <p>À terme nous voudrions proposer ce concept de parcours numérique à d'autres équipes d'ETP et ainsi
                constituer une bibliothèque d'ateliers d'ETP.</p>
            <div style="text-align:center;">
                <iframe title="Présentation adocami" class="iframevimeo" src="https://player.vimeo.com/video/454728328"
                        width="100%" height="480"
                        frameborder="0"
                        allow="autoplay;" allowfullscreen></iframe>
            </div>
            <p>Ce projet est soutenu par <a target="_blank" href="https://www.ag2rlamondiale.fr/" rel="noopener"
                                            title="Site d'AG2R LA MONDIALE">AG2R La Mondiale </a> et <a target="_blank"
                                                                                                        href="https://www.bpifrance.fr/"
                                                                                                        rel="noopener"
                                                                                                        title="Site de bpifrance">BpiFrance</a>
            </p>
            <div class="logos">
                <a target="_blank" href="https://www.ag2rlamondiale.fr" rel="noopener" title="Site d'AG2R LA MONDIALE"
                   class="inlineLink noDecoLink"><img
                            src="assets/medias/images/logoAG2R.png" alt="Logo de AG2R LA MONDIALE"/></a>
                <a target="_blank" href="https://www.bpifrance.fr/" rel="noopener" title="Site de bpifrance"
                   class="inlineLink noDecoLink"><img
                            src="assets/medias/images/Logo-BPI.jpg" alt="logo de bpifrance"/></a>
            </div>
        </section>
        <section id="catalogues">
            <h3>Catalogues en ligne</h3>
            <p>Nous vous accompagnons dans la réalisation et la mise en ligne de vos catalogues. Deux solutions sont
                envisageables :</p>
            <ul class="default studit">
                <li>Le catalogue de notre partenaire <a target="_blank" href="http://www.webpublication.fr/"
                                                        rel="noopener"
                                                        title="Webpublication : La solution professionnelle de création de contenus digitaux enrichis !">Webpublication</a>
                    au sein
                    duquel nous intégrons votre PDF, puis dessinons les zones actives, tout en leur attribuant des
                    comportements,
                </li>
                <li>Notre reader d'epub online (HTML) qui nous permet d'importer une version "mise en page" de votre
                    catalogue et d'y ajouter toutes sortes d'enrichissements, tout en restant compatible avec les
                    navigateurs desktop et mobiles.
                </li>
            </ul>
        </section>
    </div>
    <div class="box container">
        <h2>L'entreprise</h2>
        <section>
            <h3>Historique</h3>
            <p>
                STUDIT est née de la volonté de Sébastien MAILLARD et Erwannig LOUF de développer une startup basée sur
                l'historique pôle multimédia de STDI. Travaillant alors depuis respectivement 2008 et 2010 chez STDI à
                la création de ce pôle, ils ont décidé de s'installer à la technopole de Laval, afin de profiter d'un
                écosystème dynamique de jeunes entreprises et d'écoles d'ingénieurs. L'incubation par LMT (Laval Mayenne
                Technopole) a commencé en Septembre 2017 et se poursuit avec succès. STUDIT a ainsi atteint le stade
                d'entreprise "pépite" de l'incubateur de Laval en Octobre 2019.<br>De nombreux projets sont nés,
                toujours
                dans le domaine scolaire, mais également en e-santé, en secourisme, en lutte contre l'illetrisme, preuve
                de la réussite de la diversification de l'activité.
            </p>
            <div class="imgHabill">
                <div><img src="assets/medias/images/IMG_0578.jpeg" alt="Sébastien Maillard et Erwannig Louf"/></div>
                <ul class="default studit">
                    <li>Avril 2017 : Création de STUDIT</li>
                    <li>Septembre 2017 : Incubation à LMT (Laval Mayenne Technopole)</li>
                    <li>Janvier 2018 : Début du projet de plateforme numérique d'ETP</li>
                    <li>Mai 2019 : Présentation de la V1 de Virtual ETP, soutien affiché de
                        <a target="_blank" href="https://www.ag2rlamondiale.fr/" rel="noopener"
                           title="Site d'AG2R LA MONDIALE">AG2R La Mondiale </a>et
                        <a target="_blank" rel="noopener" title="Site de bpifrance" href="https://www.bpifrance.fr/">BpiFrance</a>
                    </li>
                    <li>Octobre 2019 : mise en ligne du premier atelier numérique</li>
                </ul>
            </div>
        </section>
        <section>
            <h3>Ils nous font confiance</h3>
            <p>Ces dernières années passées à STDI (depuis 2008) et à STUDIT (depuis 2017) nous permettent de travailler
                avec de grands noms de l'éditions Française. Nous sommes fiers de ce carnet d'adresses. La fidélité de
                nos clients est pour nous la meilleure preuve de la qualité de notre travail !</p>
            <div class="logosRond">
                <div class="rondLogos">
                    <a target="_blank" href="https://www.editions-hatier.fr/" rel="noopener"
                       title="Site des éditions Hatier"><img
                                src="assets/medias/images/logoHatier.png" alt="Logo de Hatier"/></a>
                </div>
                <div class="rondLogos">
                    <a target="_blank" href="https://www.editions-foucher.fr/" rel="noopener"
                       title="Site des éditions Foucher"><img
                                src="assets/medias/images/logofoucher.png" alt="Logo de Foucher"/></a>
                </div>
                <div class="rondLogos">
                    <a target="_blank" href="https://www.editions-retz.com/connaitre.html" rel="noopener"
                       title="Site des éditions RETZ"><img
                                src="assets/medias/images/logo-retz.png" alt="Logo des éditions RETZ"/></a>
                </div>
                <div class="rondLogos">
                    <a target="_blank" href="https://www.magnard.fr/" rel="noopener"
                       title="Site des éditions MAGNARD"><img
                                src="assets/medias/images/logomagnard.jpg" alt="Logo de MAGNARD"/></a>
                </div>
                <div class="rondLogos">
                    <a target="_blank" href="https://www.afadec.fr/" rel="noopener" title="Plateforme de l'AFADEC"><img
                                src="assets/medias/images/logo_enseignement-catholique.jpg"
                                alt="Logo de l'enseignement catholique"/></a>
                </div>
                <div class="rondLogos">
                    <a target="_blank" href="https://www.ag2rlamondiale.fr" rel="noopener"
                       title="Site de AG2R LA MONDIALE"><img
                                src="assets/medias/images/logo-ag2r-la-mondiale.svg"
                                alt="Logo de AG2R LA MONDIALE"/></a>
                </div>
                <div class="rondLogos">
                    <a target="_blank" href="https://www.editions-bordas.fr/" rel="noopener"
                       title="Site des éditions bordas"><img
                                src="assets/medias/images/Logo_Bordas.png" alt="Logo de bordas éditeur"/></a>
                </div>
                <div class="rondLogos">
                    <a target="_blank" href="https://www.bpifrance.fr/" rel="noopener" title="Site de bpifrance"><img
                                src="assets/medias/images/Logo-BPI.jpg" alt="Logo de bpifrance"/></a>
                </div>
                <div class="rondLogos">
                    <a target="_blank" href="https://www.editionsdidier.com/fr" rel="noopener"
                       title="Site des éditions didier"><img
                                src="assets/medias/images/logoDidier.jpg" alt="Logo des éditions didier"/></a>
                </div>
                <div class="rondLogos">
                    <a target="_blank" href="https://www.hachette-education.com/" rel="noopener"
                       title="Site des éditions Hachette"><img
                                src="assets/medias/images/logo-hachette-education.jpg"
                                alt="Logo de hachette ÉDUCATION"/></a>
                </div>
                <div class="rondLogos">
                    <a target="_blank" href="https://www.hec.edu/fr" rel="noopener" title="Site de HEC Paris"><img
                                src="assets/medias/images/HEC-Logo.jpg" alt="Logo de HEC Paris"/></a>
                </div>
                <div class="rondLogos">
                    <a target="_blank" href="https://www.cmpr-bagnoles.com/" rel="noopener"
                       title="Site du CMPR du Bagnoles de L'orne"><img
                                src="assets/medias/images/logo-cmpr-bagnole.png"
                                alt="Logo du CMPR du Bagnoles de L'orne"/></a>
                </div>
            </div>
        </section>
    </div>
</main>

<!-- Footer -->
<footer>
    <div class="container 75%">
        <h2>Nous contacter</h2>
        <p>Complétez les champs ci-dessous et nous vous recontacterons rapidement.</p>

        <?php
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['recaptcha_response'])) {

            // Build POST request:
            $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
            $recaptcha_secret = '6Lc4f6cUAAAAAFnwKcUVqchiVs9uGQCPGM2GGZTj';
            $recaptcha_response = $_POST['recaptcha_response'];

            // Make and decode POST request:
            $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
            $recaptcha = json_decode($recaptcha);

            if ($recaptcha->score >= 0.5) {
                if (isset($_POST["email"]) && isset($_POST["message"]) && isset($_POST["name"])) {
                    if ($_POST["email"] != "" && $_POST["message"] != "" && $_POST["name"] != "") {
                        try {
                            $querySend = $connect->prepare("INSERT INTO messages(nom, email, message) VALUES (:nom, :email, :message)");
                            $querySend->bindParam(":nom", $_POST['name']);
                            $querySend->bindParam(":email", $_POST['email']);
                            $querySend->bindParam(":message", $_POST['message']);
                            $querySend->execute();

                            ini_set("SMTP", "studit-vm01.nfrance.net");
                            ini_set("sendmail_from", "erwannig@studit.fr");

                            if (preg_match("/^([\w-\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,4})/i", $_POST["email"])) {
                                $headers = "From: " . $_POST["email"];
                                $message = $_POST["message"];
                            } else {
                                $headers = "From: erwannig@studit.fr";
                                $message = "Mail de l'expéditeur : " . $_POST["email"] . '\r\n' . $_POST["message"];
                            }

                            mail("contact@studit.fr", '[SITE] ' . $_POST["name"], $message, $headers);
                            echo '<div class="msgSuccess">Votre message a bien été envoyé</div>';
                        } catch (Exception $exception) {
                            echo '<div class="error">Erreur dans le formulaire</div>';
                        }

                    } else {
                        echo '<div class="error">Tous les champs doivent être remplis</div>';
                    }
                }
            } else {
                echo '<div class="error">Vous avez été identifié comme un Robot !</div>';
            }
        }
        ?>

        <form method="post" action="index.php" id="messageForm">
            <div class="row">
                <div class="formPart1">
                    <div>
                        <label for="name">Nom</label>
                        <input type="text" id="name" name="name"
                               value="<?php echo isset($_POST['name']) ? $_POST['name'] : '' ?>"/>
                    </div>
                    <div>
                        <label for="email">Email</label>
                        <input type="email" id="email" name="email"
                               value="<?php echo isset($_POST['email']) ? $_POST['email'] : '' ?>"/>
                    </div>
                </div>
            </div>
            <div class="formPart2">
                <div>
                    <label for="message">Message</label>
                    <textarea id="message" name="message"
                              rows="6"><?php echo isset($_POST['message']) ? $_POST['message'] : '' ?></textarea>
                </div>
            </div>
            <div>
                <div>
                    <ul class="actions">
                        <li><input type="submit" value="Envoyer"/></li>
                    </ul>
                </div>
            </div>
            <input type="hidden" name="recaptcha_response" id="recaptchaResponse" value=""/>
        </form>

        <ul class="copyright">
            <li>&copy; Studit. Tous droits réservés.</li>
            <li>Design: <a href="http://www.studit.fr">STUDIT</a></li>
        </ul>
    </div>
</footer>

<!--[if lte IE 8]>
<script src="assets/js/ie/respond.min.js"></script><![endif]-->
<script src="assets/js/studit.min.js"></script>


<!-- Recaptcha -->
<script src="https://www.google.com/recaptcha/api.js?render=6Lc4f6cUAAAAADuqS8doN-Vr2LIm2J_hnwooPg3c" async
        defer></script>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        document.querySelector("#messageForm").addEventListener("submit", (e) => {
            e.preventDefault();
            grecaptcha.ready(function () {
                grecaptcha.execute('6Lc4f6cUAAAAADuqS8doN-Vr2LIm2J_hnwooPg3c', {action: 'contact'}).then(function (token) {
                    var recaptchaResponse = document.getElementById('recaptchaResponse');
                    recaptchaResponse.value = token;
                    document.querySelector("#messageForm").submit();
                });
            });
        });
    }, false);
</script>
</body>
</html>
