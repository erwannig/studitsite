<?php

$host = "localhost";

/*********************************
 * /   Conf de la base destination
 ********************************/
$user = "root";
$password = "";
$database = "studit";

try {
    $connect = new PDO("mysql:host=$host;dbname=$database", $user, $password);
} catch (PDOException $pe) {
    die("Could not connect to the database $database :" . $pe->getMessage());
}
