/**
 * Created by stdi on 28/04/2017.
 */

function animate(elem, yOffset = 0, link = false) {
    let item = document.querySelector(link ? elem.getAttribute("href") : elem);
    const y = item.getBoundingClientRect().top + window.pageYOffset + yOffset;
    window.scrollTo({top: y, behavior: 'smooth'});
}

function checkMenu(ev) {
    if (window.pageYOffset > 60 && !document.querySelector('nav').classList.contains('test')) {
        document.querySelector('nav').classList.add('test');
        document.querySelector('.logo-nav img').style.display = "inline";
    } else if (window.pageYOffset <= 60) {
        document.querySelector('nav').classList.remove('test');
        document.querySelector('.logo-nav img').style.display = "none";
    }
}

document.addEventListener('DOMContentLoaded', () => {

    checkMenu();
    document.querySelectorAll('a.aNav').forEach((navLink) => {
        navLink.addEventListener('click', (event) => {
            event.preventDefault();
            event.stopPropagation();
            animate(navLink, -80, true);
        }, false);
    })

    window.addEventListener('scroll', checkMenu, false);

    document.querySelectorAll('.presentation .content').forEach((item) => {
        item.addEventListener('click', function (evt) {
            evt.preventDefault();
            let link = evt.currentTarget.getAttribute('datalien');
            animate(link, -80);
        }, false);
    });

    document.querySelectorAll('.btnMenu').forEach(function (btn) {
        btn.addEventListener('click', function (ev) {
            ev.stopPropagation();
            let nav = document.querySelector("nav");
            if (nav.getAttribute('data-menuActif') === "true") {
                nav.setAttribute('data-menuActif', "false");
                let b = document.getElementById("btnIcon");
                if (b.classList.contains('fa-times')) {
                    b.classList.remove('fa-times');
                    b.classList.add('fa-bars');
                }
            } else {
                nav.setAttribute('data-menuActif', "true");
                let b = document.getElementById("btnIcon");
                if (b.classList.contains('fa-bars')) {
                    b.classList.remove('fa-bars');
                    b.classList.add('fa-times');
                }
            }
        })
    });
}, false);
